(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room

(menu-bar-mode -1)            ; Disable the menu bar

;; Set up the visible bell (setq visible-bell t)

(set-face-attribute 'default nil :font "Iosevka:size=20")

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
 (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
   (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package command-log-mode)

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

(setq evil-want-keybinding nil)
(use-package evil)
(evil-mode t)
(use-package evil-collection)
(use-package evil-commentary)
(use-package evil-escape)
(use-package evil-easymotion)
(use-package evil-mc)
(use-package evil-python-movement)
(use-package evil-smartparens)
(smartparens-global-mode)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("3d2e532b010eeb2f5e09c79f0b3a277bfc268ca91a59cdda7ffd056b868a03bc" "2dd4951e967990396142ec54d376cced3f135810b2b69920e77103e0bcedfba9" "6945dadc749ac5cbd47012cad836f92aea9ebec9f504d32fe89a956260773ca4" "89d9dc6f4e9a024737fb8840259c5dd0a140fd440f5ed17b596be43a05d62e67" "aec7b55f2a13307a55517fdf08438863d694550565dee23181d2ebd973ebd6b8" default))
 '(frame-brackground-mode 'dark)
 '(ispell-dictionary nil)
 '(package-selected-packages
   '(hl-todo company-go go-mode ido-mode typescript-mode fixmee keychain-environment keychain-enviroment keychain-enviromen dashboard all-the-icons page-break-lines magit projectile company-box lsp-ivy lsp-mode syntax-subword rustic nim-mode auto-sudoedit zenburn gruber-darker-theme doom-themes multiple-cursors color-identifiers-mode company-irony irony company fira-code-mode linum-relative evil-leader evil-smartparens evil-python-movement evil-mc evil-easymotion evil-escape evil-commentary evil-collection evil doom-modeline ivy command-log-mode use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(use-package evil-leader)
(evil-leader/set-leader "<SPC>")

(evil-set-initial-state 'dired-mode 'emacs)

(ivy-mode t)

(setq display-line-numbers-type 'relative)

;; activate line numbering in all buffers/modes
(global-display-line-numbers-mode)

(defun add-to-map(keys func)
  "Add a keybinding in evil mode from keys to func."
  (define-key evil-normal-state-map (kbd keys) func)
  (define-key evil-motion-state-map (kbd keys) func))

(add-to-map "<SPC>" nil)

(add-to-map "<SPC> ." 'find-file)
(add-to-map "<SPC> d" 'dired)
(add-to-map "<SPC> c" 'compile)
(add-to-map "<SPC> g g" 'magit-status-quick)
(add-to-map "<SPC> w" 'whitespace-mode)
(add-to-map "<SPC> r" 'revert-buffer-quick)

(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "C-c k") 'kill-compilation)
(global-set-key (kbd "C-c n") 'next-error)

(use-package color-identifiers-mode)
(add-hook 'after-init-hook 'global-color-identifiers-mode)

(use-package multiple-cursors)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

(use-package auto-sudoedit)
(auto-sudoedit-mode 1)

(use-package rustic)
(use-package rust-mode
  :hook (rust-mode . lsp-deferred))
(use-package syntax-subword)

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init)
  ;(lsp-enable-which-key-integration t))

(global-syntax-subword-mode)

(use-package lsp-ivy)

(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-go)

(add-hook 'go-mode-hook (lambda () (setq tab-width 4)))

;(use-package company-box
;  :hook (company-mode . company-box-mode))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/Projects")
    (setq projectile-project-search-path '("~/Projects")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package irony)
(use-package company-irony)

(add-hook 'after-init-hook 'global-company-mode)

(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)

(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

(add-hook 'dired-mode-hook (lambda () (text-scale-increase 2)))
(add-hook 'c-mode-hook (lambda () (text-scale-increase 2)))
(add-hook 'markdown-mode-hook (lambda () (text-scale-increase 2)))
(add-hook 'makefile-mode-hook (lambda () (text-scale-increase 2)))
(add-hook 'rustic-mode-hook (lambda () (text-scale-increase 2)))
(add-hook 'nim-mode-hook (lambda () (text-scale-increase 2)))
(add-hook 'rcake-mode-hook (lambda () (text-scale-increase 2)))
(add-hook 'cang-mode-hook (lambda () (text-scale-increase 2)))

(use-package page-break-lines)
(use-package all-the-icons)
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

(setq dashboard-startup-banner 'logo)

(use-package gruber-darker-theme)
(use-package zenburn-theme)

(load-theme 'gruber-darker t)
(load-file "~/.emacs.d/rcake-mode.el")
(load-file "~/.emacs.d/cang-mode.el")

(setq magit-display-buffer-function #'magit-display-buffer-fullframe-status-v1)
(setq projectile-switch-project-action 'magit-status)

(setq whitespace-style
      '(face spaces tabs space-mark tab-mark))

(use-package keychain-environment)
(keychain-refresh-environment)

(setq confirm-kill-emacs 'y-or-n-p)

(use-package fixmee)
(use-package button-lock)

(global-fixmee-mode)

(use-package typescript-mode)

(setq-default indent-tabs-mode nil)

(add-hook 'asm-mode-hook
          (lambda()
            (setq indent-tabs-mode t)))

(use-package hl-todo)
(global-hl-todo-mode)

(defun colorize-compilation-buffer ()
  (toggle-read-only)
  (ansi-color-apply-on-region compilation-filter-start (point))
  (toggle-read-only))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)
